#pragma once

#include <iostream>


class CList {
private:
    int number;
    CList *list;
public:
    CList(int length);

    ~CList();

    void print(std::ostream &ostream) const;
};