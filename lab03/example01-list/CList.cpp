#include "CList.h"


void CList::print(std::ostream &ostream) const 
{
    ostream << number << std::endl;
    if(list != nullptr)
    {
        list->print(ostream);
    }
}

CList::CList(int length) 
{
    number = length;
    if(length != 1)
    {
        list = new CList(length - 1);
    }
    else
    {
        list = nullptr;
    }
}

CList::~CList() 
{
    delete list;
}

