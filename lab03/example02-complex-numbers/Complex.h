//
// Created by krupito5 on 3/6/24.
//

#ifndef LAB03_2_COMPLEX_H
#define LAB03_2_COMPLEX_H

#include <iostream>

class Complex
{
public:
    Complex(float real, float imaginary);
    Complex(float real);
    friend void operator <<(std::ostream& o, const Complex& c);
    friend Complex operator+(const Complex& c1, const Complex& c2);
    //stihli jsme pouze operátor +
private:
    float m_Real;
    float m_Imaginary;
};


#endif //LAB03_2_COMPLEX_H
