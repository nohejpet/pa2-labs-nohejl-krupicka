//
// Created by krupito5 on 3/6/24.
//

#include "Complex.h"

Complex::Complex(float real, float imaginary) : m_Real(real), m_Imaginary(imaginary)
{
}

Complex::Complex(float real) : m_Real(real), m_Imaginary(0)
{
}

void operator<<(std::ostream &o, const Complex &c)
{
    o << "[" << c.m_Real << ", " << c.m_Imaginary << "]";
}

Complex operator+(const Complex &c1, const Complex &c2) {
    return {c1.m_Real + c2.m_Real, c1.m_Imaginary + c2.m_Imaginary};
}
