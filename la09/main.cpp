#ifndef __TRAINER__
#include <cassert>
#include <vector>
#include <string>
#include <queue>
#include <tuple>
#include <utility>
#include <map>
#include <set>
#include <optional>
#endif

using Grid = std::vector<std::string>;
using Coord = std::pair<int, int>;


Coord moveRight = {1,0};
Coord moveLeft = {-1,0};
Coord moveUp = {0,1};
Coord moveDown = {0,-1};

std::vector moves = {moveRight, moveLeft, moveUp, moveDown};

std::optional<unsigned> shortestPath(const Grid & g) {

    Coord start = {0,0};
    Coord max = {g[0].size(), g.size()};

    for(const auto& row : g)
    {
        size_t startIndex = row.find("s");
        if(startIndex != std::string::npos)
        {
            start.first = startIndex;
            break;
        }
        start.second++;
    }


    std::map<Coord, size_t> visited;
    visited[start] = 0;

    std::queue<Coord> bfsQueue;
    bfsQueue.push(start);

    while(!bfsQueue.empty())
    {
        auto current = bfsQueue.front();
        bfsQueue.pop();

        if(g[current.second][current.first] == 'e')
        {
            return visited[current];
        }

        for(const auto& move : moves)
        {
            if(current.first + move.first < 0 || current.second + move.second < 0)
            {
                continue;
            }
            if(current.first + move.first >= max.first
            || current.second + move.second >= max.second
            || g[current.second + move.second][current.first] == '#'
            || g[current.second][current.first + move.first] == '#')
            {
                continue;
            }

            Coord newCurrent = {current.first + move.first, current.second + move.second};
            if(visited.find(newCurrent) == visited.end())
            {
                int cnt = visited[current];
                visited[newCurrent] = cnt + 1;
                bfsQueue.push(newCurrent);
            }
        }
    }



    return std::nullopt;
}

#ifndef __TRAINER__
int main() {
    assert(shortestPath({
        "#####",
        "#s e#",
        "#####",
    }) == 2);
    assert(shortestPath({
        " ",
        "s",
        " ",
        " ",
        " ",
        " ",
        "e",
    }) == 5);
    assert(!shortestPath({
        "############################",
        "#s                   #    e#",
        "############################",
    }));
    assert(!shortestPath({
        "s #  ",
        "### e",
    }));
    assert(shortestPath({
        "     ",
        " s#e ",
        " ##  ",
    }) == 4);
    assert(!shortestPath({
        "s#e",
    }));
    assert(shortestPath({
        "#########",
        "#   #   #",
        "# # # # #",
        "#s#   #e#",
        "#########",
    }) == 14);
    assert(shortestPath({
        "       ",
        " ####  ",
        "   s## ",
        "###  #e",
    }) == 14);
    assert(!shortestPath({
        "s    #     e",
    }));
    assert(!shortestPath({
        "e#",
        "#s",
    }));
    assert(shortestPath({
        "s #    #  ",
        " #   ## # ",
        " #   #    ",
        "          ",
        "#### # #  ",
        "       #  ",
        "  ##### e ",
        "          ",
    }) == 14);

    return EXIT_SUCCESS;
}
#endif