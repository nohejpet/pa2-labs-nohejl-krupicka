#pragma once

#include "CItem.h"

class COxygenExplosive : public CItem {
public:
    explicit COxygenExplosive(int id);

    bool willExplodeIn(CEnvironment & environment) const override;
};