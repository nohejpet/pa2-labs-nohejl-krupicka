//
// Created by krupito5 on 4/10/24.
//

#include "CTemperatureExplosive.h"

int CTemperatureExplosive::getMaxTemperature() const {
    return m_MaxTemperature;
}

CTemperatureExplosive::CTemperatureExplosive(int id, int maxTemperature): CItem(id), m_MaxTemperature(maxTemperature){}

bool CTemperatureExplosive::willExplodeIn(CEnvironment &environment) const {
    return environment.getTemperature() > getMaxTemperature();
}

