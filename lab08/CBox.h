#pragma once

#include "CItem.h"

class CBox : public CItem{
private:
    std::vector<CItem *> m_items;
public:
    CBox(int id);
    bool willExplodeIn(CEnvironment & environment) const override;

    CBox & insert(CItem * item);
};
