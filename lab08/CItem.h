#pragma once

class CEnvironment;

#include "CEnvironment.h"

class CItem {
private:
    int m_Id;
public:
    CItem(int id);
    virtual ~CItem() = default;
    virtual bool willExplodeIn(CEnvironment & environment) const = 0;

    int getId() const;
};