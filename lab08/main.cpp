#include <iostream>
#include "CBox.h"
#include "CItem.h"
#include "CNoExplosive.h"
#include "CTemperatureExplosive.h"
#include "COxygenExplosive.h"

int main() {

    CEnvironment env1(10, false);
    CItem * item = new CNoExplosive(15);
    CItem * item2 = new CTemperatureExplosive(1, 50);
    CItem * item3 = new COxygenExplosive(42);

    CBox * box = new CBox(666);

    box->insert(item);
    box->insert(item2);
    box->insert(item3);

    env1.insert(box);

    std::cout << "All okay!" << std::endl;
    delete item;
    delete item2;
    delete item3;
    delete box;
    return 0;
}
