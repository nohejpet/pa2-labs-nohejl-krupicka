//
// Created by krupito5 on 4/10/24.
//

#include "CBox.h"

CBox::CBox(int id) : CItem(id) {

}

CBox &CBox::insert(CItem *item) {
    m_items.push_back(item);
    return *this;
}

bool CBox::willExplodeIn(CEnvironment &environment) const {
    for(const CItem * item : m_items){
        if(item->willExplodeIn(environment)){
            return true;
        }
    }
    return false;
}
