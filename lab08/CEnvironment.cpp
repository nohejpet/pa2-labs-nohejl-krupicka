//
// Created by krupito5 on 4/10/24.
//

#include <iostream>
#include "CEnvironment.h"

CEnvironment::CEnvironment(int temperature, bool oxygen) : m_temperature(temperature), m_oxygen(oxygen) {}

int CEnvironment::getTemperature() const {
    return m_temperature;
}

bool CEnvironment::hasOxygen() const {
    return m_oxygen;
}

CEnvironment & CEnvironment::insert(CItem *item) {
    if(!canInsert(item)){
        std::cerr << item->getId() << " BOOOOOOOOOOOOOOM" << std::endl;
        throw std::invalid_argument("Now you're dead");
    }
    m_items.push_back(item);
    return * this;
}

bool CEnvironment::canInsert(CItem * item) {
    return !item->willExplodeIn(*this);
}
