//
// Created by krupito5 on 4/10/24.
//

#include "CNoExplosive.h"

bool CNoExplosive::willExplodeIn(CEnvironment & environment) const {
    return false;
}

CNoExplosive::CNoExplosive(int id): CItem(id) {}
