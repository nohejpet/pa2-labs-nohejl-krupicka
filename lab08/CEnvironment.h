#pragma once

#include <vector>

class CItem;

#include "CItem.h"

class CEnvironment {
private:
    int m_temperature;
    bool m_oxygen;

    std::vector<const CItem *> m_items;
public:
    CEnvironment(int temperature, bool oxygen);

    int getTemperature() const;
    bool hasOxygen() const;

    CEnvironment & insert(CItem * item);

    bool canInsert(CItem * item);
};