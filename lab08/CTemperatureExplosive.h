#pragma once

#include "CItem.h"

class CTemperatureExplosive : public CItem{
private:
    int m_MaxTemperature;
public:
    CTemperatureExplosive(int id, int maxTemperature);
    int getMaxTemperature() const;
    bool willExplodeIn(CEnvironment & environment) const override;
};