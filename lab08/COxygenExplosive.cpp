//
// Created by krupito5 on 4/10/24.
//

#include "COxygenExplosive.h"

COxygenExplosive::COxygenExplosive(int id) : CItem(id) {}

bool COxygenExplosive::willExplodeIn(CEnvironment & environment) const {
    return environment.hasOxygen();
}
