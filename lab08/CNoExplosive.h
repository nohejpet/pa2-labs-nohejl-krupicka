#pragma once


#include "CItem.h"

class CNoExplosive : public CItem {

public:
    explicit CNoExplosive(int id);

    bool willExplodeIn(CEnvironment & environment) const override;
};