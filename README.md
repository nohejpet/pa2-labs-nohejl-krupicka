# PA2-labs-nohejl-krupicka



## Úvod

Zde najdete informace a kódy ke cvičením, které již tento semestr proběhli. Je třeba zmínit, že kódy jsou ve fázi,
ve které jsme je zanechali na konci cvičení, úlohy tedy často nejsou dodělané.


## Další informace

- [ ] [odkaz na další informace o našem cvičení](https://courses.fit.cvut.cz/BI-PA2/teacher/nohejpet/index.html)
