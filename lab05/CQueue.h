#pragma once

struct Node{
    int data;
    Node * next;
};

class CQueue {

public:

    CQueue();

    CQueue(const CQueue & other);

    ~CQueue();

    CQueue & operator=(const CQueue & other);

    // konstruktory, destruktory

    void push(int x);

    int pop();

    bool isEmpty() const;

    int size() const;

    const int & front() const;

    const int & back() const;

private:
    Node * m_front;
    Node * m_back;
    int m_size;

    void clear();
    void pushAll(const CQueue & other);
};